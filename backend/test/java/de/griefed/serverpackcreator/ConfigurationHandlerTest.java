package de.griefed.serverpackcreator;

import de.griefed.serverpackcreator.versionmeta.VersionMeta;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import net.lingala.zip4j.ZipFile;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

class ConfigurationHandlerTest {

  private final ApplicationProperties applicationProperties;
  private final ConfigurationHandler configurationHandler;
  private final VersionMeta versionMeta;
  String[] args = new String[]{"--setup", "backend/test/resources/serverpackcreator.properties"};

  ConfigurationHandlerTest() throws IOException, ParserConfigurationException, SAXException {
    applicationProperties = ServerPackCreator.getInstance(args).getApplicationProperties();
    configurationHandler = ServerPackCreator.getInstance(args).getConfigurationHandler();
    versionMeta = ServerPackCreator.getInstance(args).getVersionMeta();
  }

  @Test
  void checkConfigFileTest() {
    Assertions.assertFalse(
        configurationHandler.checkConfiguration(
            new File("backend/test/resources/testresources/spcconfs/serverpackcreator.conf"),
            false));
  }

  @Test
  void isDirTestCopyDirs() {
    Assertions.assertTrue(
        configurationHandler.checkConfiguration(
            new File(
                "backend/test/resources/testresources/spcconfs/serverpackcreator_copydirs.conf"),
            false));
  }

  @Test
  void isDirTestJavaPath() {
    Assertions.assertFalse(
        configurationHandler.checkConfiguration(
            new File(
                "backend/test/resources/testresources/spcconfs/serverpackcreator_javapath.conf"),
            false));
  }

  @Test
  void isDirTestMinecraftVersion() {
    Assertions.assertTrue(
        configurationHandler.checkConfiguration(
            new File(
                "backend/test/resources/testresources/spcconfs/serverpackcreator_minecraftversion.conf"),
            false));
  }

  @Test
  void isModLoaderLegacyFabric() {
    Assertions.assertFalse(
        configurationHandler.checkConfiguration(
            new File(
                "backend/test/resources/testresources/spcconfs/serverpackcreator_legacyfabric.conf"),
            false));
  }

  @Test
  void isModLoaderQuilt() {
    Assertions.assertFalse(
        configurationHandler.checkConfiguration(
            new File("backend/test/resources/testresources/spcconfs/serverpackcreator_quilt.conf"),
            false));
  }

  @Test
  void isDirTestModLoaderFalse() {
    Assertions.assertTrue(
        configurationHandler.checkConfiguration(
            new File(
                "backend/test/resources/testresources/spcconfs/serverpackcreator_modloaderfalse.conf"),
            false));
  }

  @Test
  void isDirTestModLoaderVersion() {
    Assertions.assertTrue(
        configurationHandler.checkConfiguration(
            new File(
                "backend/test/resources/testresources/spcconfs/serverpackcreator_modloaderversion.conf"),
            false));
  }

  @Test
  void checkModpackDirTest() {
    String modpackDirCorrect = "./backend/test/resources/forge_tests";
    Assertions.assertTrue(
        configurationHandler.checkModpackDir(modpackDirCorrect, new ArrayList<>(100)));
  }

  @Test
  void checkModpackDirTestFalse() {
    Assertions.assertFalse(
        configurationHandler.checkModpackDir("modpackDir", new ArrayList<>(100)));
  }

  @Test
  void checkCopyDirsTest() {
    String modpackDir = "backend/test/resources/forge_tests";
    List<String> copyDirs =
        new ArrayList<>(Arrays.asList("config", "mods", "scripts", "seeds", "defaultconfigs"));
    Assertions.assertTrue(
        configurationHandler.checkCopyDirs(copyDirs, modpackDir, new ArrayList<>(100)));
  }

  @Test
  void checkCopyDirsTestFalse() {
    String modpackDir = "backend/test/resources/forge_tests";
    List<String> copyDirsInvalid =
        new ArrayList<>(Arrays.asList("configs", "modss", "scriptss", "seedss", "defaultconfigss"));
    Assertions.assertFalse(
        configurationHandler.checkCopyDirs(copyDirsInvalid, modpackDir, new ArrayList<>(100)));
  }

  @Test
  void checkCopyDirsTestFiles() {
    String modpackDir = "backend/test/resources/forge_tests";
    List<String> copyDirsAndFiles =
        new ArrayList<>(
            Arrays.asList(
                "config",
                "mods",
                "scripts",
                "seeds",
                "defaultconfigs",
                "test.txt;test.txt",
                "test2.txt;test2.txt"));
    Assertions.assertTrue(
        configurationHandler.checkCopyDirs(copyDirsAndFiles, modpackDir, new ArrayList<>(100)));
  }

  @Test
  void checkCopyDirsTestFilesFalse() {
    String modpackDir = "backend/test/resources/forge_tests";
    List<String> copyDirsAndFilesFalse =
        new ArrayList<>(
            Arrays.asList(
                "configs",
                "modss",
                "scriptss",
                "seedss",
                "defaultconfigss",
                "READMEee.md;README.md",
                "LICENSEee;LICENSE",
                "LICENSEee;test/LICENSE",
                "LICENSEee;test/license.md"));
    Assertions.assertFalse(
        configurationHandler.checkCopyDirs(
            copyDirsAndFilesFalse, modpackDir, new ArrayList<>(100)));
  }

  @Test
  void checkModloaderTestForge() {
    Assertions.assertTrue(configurationHandler.checkModloader("Forge"));
    Assertions.assertTrue(configurationHandler.checkModloader("fOrGe"));
    Assertions.assertTrue(configurationHandler.checkModloader("Fabric"));
    Assertions.assertTrue(configurationHandler.checkModloader("fAbRiC"));
    Assertions.assertTrue(configurationHandler.checkModloader("Quilt"));
    Assertions.assertTrue(configurationHandler.checkModloader("qUiLt"));
    Assertions.assertTrue(configurationHandler.checkModloader("lEgAcYfAbRiC"));
    Assertions.assertTrue(configurationHandler.checkModloader("LegacyFabric"));

    Assertions.assertFalse(configurationHandler.checkModloader("modloader"));
  }

  @Test
  void checkModloaderVersionTestForge() {
    Assertions.assertTrue(configurationHandler.checkModloaderVersion("Forge", "36.1.2", "1.16.5"));
    Assertions.assertFalse(configurationHandler.checkModloaderVersion("Forge", "90.0.0", "1.16.5"));
  }

  @Test
  void checkModloaderVersionTestFabric() {
    Assertions.assertTrue(configurationHandler.checkModloaderVersion("Fabric", "0.11.3", "1.16.5"));
    Assertions.assertFalse(
        configurationHandler.checkModloaderVersion("Fabric", "0.90.3", "1.16.5"));
  }

  @Test
  void checkModloaderVersionTestQuilt() {
    Assertions.assertTrue(configurationHandler.checkModloaderVersion("Quilt", "0.16.1", "1.16.5"));
    Assertions.assertFalse(configurationHandler.checkModloaderVersion("Quilt", "0.90.3", "1.16.5"));
  }

  @Test
  void isLegacyFabricVersionCorrectTest() {
    Assertions.assertTrue(
        configurationHandler.checkModloaderVersion("LegacyFabric", "0.13.3", "1.12.2"));
    Assertions.assertFalse(
        configurationHandler.checkModloaderVersion("LegacyFabric", "0.999.3", "1.12.2"));
  }

  @Test
  void checkConfigModelTest() {
    List<String> clientMods =
        new ArrayList<>(
            Arrays.asList(
                "AmbientSounds",
                "BackTools",
                "BetterAdvancement",
                "BetterPing",
                "cherished",
                "ClientTweaks",
                "Controlling",
                "DefaultOptions",
                "durability",
                "DynamicSurroundings",
                "itemzoom",
                "jei-professions",
                "jeiintegration",
                "JustEnoughResources",
                "MouseTweaks",
                "Neat",
                "OldJavaWarning",
                "PackMenu",
                "preciseblockplacing",
                "SimpleDiscordRichPresence",
                "SpawnerFix",
                "TipTheScales",
                "WorldNameRandomizer"));
    List<String> copyDirs =
        new ArrayList<>(Arrays.asList("config", "mods", "scripts", "seeds", "defaultconfigs"));
    ConfigurationModel configurationModel = new ConfigurationModel();
    configurationModel.setModpackDir("./backend/test/resources/forge_tests");
    configurationModel.setClientMods(clientMods);
    configurationModel.setCopyDirs(copyDirs);
    configurationModel.setIncludeServerInstallation(true);
    configurationModel.setIncludeServerIcon(true);
    configurationModel.setIncludeServerProperties(true);
    configurationModel.setIncludeZipCreation(true);
    configurationModel.setModLoader("Forge");
    configurationModel.setModLoaderVersion("36.1.2");
    configurationModel.setMinecraftVersion("1.16.5");
    Assertions.assertFalse(configurationHandler.checkConfiguration(configurationModel, false));

    Assertions.assertTrue(
        configurationModel.getScriptSettings().containsKey("SPC_MINECRAFT_SERVER_URL_SPC"));
    Assertions.assertTrue(
        configurationModel.getScriptSettings().containsKey("SPC_SERVERPACKCREATOR_VERSION_SPC"));
    Assertions.assertTrue(
        configurationModel.getScriptSettings().containsKey("SPC_MINECRAFT_VERSION_SPC"));
    Assertions.assertTrue(configurationModel.getScriptSettings().containsKey("SPC_MODLOADER_SPC"));
    Assertions.assertTrue(
        configurationModel.getScriptSettings().containsKey("SPC_MODLOADER_VERSION_SPC"));
    Assertions.assertTrue(configurationModel.getScriptSettings().containsKey("SPC_JAVA_ARGS_SPC"));
    Assertions.assertTrue(configurationModel.getScriptSettings().containsKey("SPC_JAVA_SPC"));
    Assertions.assertTrue(
        configurationModel.getScriptSettings().containsKey("SPC_FABRIC_INSTALLER_VERSION_SPC"));
    Assertions.assertTrue(configurationModel.getScriptSettings()
                                            .containsKey("SPC_LEGACYFABRIC_INSTALLER_VERSION_SPC"));
    Assertions.assertTrue(
        configurationModel.getScriptSettings().containsKey("SPC_QUILT_INSTALLER_VERSION_SPC"));

    Assertions.assertEquals(configurationModel.getMinecraftVersion(),
                            configurationModel.getScriptSettings()
                                              .get("SPC_MINECRAFT_VERSION_SPC"));
    Assertions.assertEquals(configurationModel.getModLoader(),
                            configurationModel.getScriptSettings().get("SPC_MODLOADER_SPC"));
    Assertions.assertEquals(configurationModel.getModLoaderVersion(),
                            configurationModel.getScriptSettings()
                                              .get("SPC_MODLOADER_VERSION_SPC"));
    Assertions.assertEquals(configurationModel.getJavaArgs(),
                            configurationModel.getScriptSettings().get("SPC_JAVA_ARGS_SPC"));
    Assertions.assertEquals("java", configurationModel.getScriptSettings().get("SPC_JAVA_SPC"));
    Assertions.assertEquals(
        versionMeta.minecraft().getServer(configurationModel.getMinecraftVersion()).get().url()
                   .get().toString(),
        configurationModel.getScriptSettings().get("SPC_MINECRAFT_SERVER_URL_SPC"));
    Assertions.assertEquals(applicationProperties.serverPackCreatorVersion(),
                            configurationModel.getScriptSettings()
                                              .get("SPC_SERVERPACKCREATOR_VERSION_SPC"));
    Assertions.assertEquals(versionMeta.fabric().releaseInstaller(),
                            configurationModel.getScriptSettings()
                                              .get("SPC_FABRIC_INSTALLER_VERSION_SPC"));
    Assertions.assertEquals(versionMeta.legacyFabric().releaseInstaller(),
                            configurationModel.getScriptSettings()
                                              .get("SPC_LEGACYFABRIC_INSTALLER_VERSION_SPC"));
    Assertions.assertEquals(versionMeta.quilt().releaseInstaller(),
                            configurationModel.getScriptSettings()
                                              .get("SPC_QUILT_INSTALLER_VERSION_SPC"));
  }

  @Test
  void zipArchiveTest() {
    ConfigurationModel configurationModel = new ConfigurationModel();
    configurationModel.setModpackDir(
        "backend/test/resources/testresources/Survive_Create_Prosper_4_valid.zip");
    Assertions.assertFalse(configurationHandler.checkConfiguration(configurationModel, true));
    configurationModel = new ConfigurationModel();
    configurationModel.setModpackDir(
        "backend/test/resources/testresources/Survive_Create_Prosper_4_invalid.zip");
    Assertions.assertTrue(configurationHandler.checkConfiguration(configurationModel, true));
  }

  @Test
  void checkConfigurationFileTest() {
    Assertions.assertFalse(
        configurationHandler.checkConfiguration(
            new File("backend/test/resources/testresources/spcconfs/serverpackcreator.conf"),
            true));
  }

  @Test
  void checkConfigurationFileAndModelTest() {
    ConfigurationModel configurationModel = new ConfigurationModel();
    Assertions.assertFalse(
        configurationHandler.checkConfiguration(
            new File("backend/test/resources/testresources/spcconfs/serverpackcreator.conf"),
            configurationModel,
            true));
    Assertions.assertEquals(
        "./backend/test/resources/forge_tests", configurationModel.getModpackDir());
    Assertions.assertEquals("1.16.5", configurationModel.getMinecraftVersion());
    Assertions.assertEquals("Forge", configurationModel.getModLoader());
    Assertions.assertEquals("36.1.2", configurationModel.getModLoaderVersion());

    configurationModel = new ConfigurationModel();
    Assertions.assertFalse(
        configurationHandler.checkConfiguration(
            new File("backend/test/resources/testresources/spcconfs/serverpackcreator.conf"),
            configurationModel,
            new ArrayList<>(),
            false));
    Assertions.assertEquals(
        "./backend/test/resources/forge_tests", configurationModel.getModpackDir());
    Assertions.assertEquals("1.16.5", configurationModel.getMinecraftVersion());
    Assertions.assertEquals("Forge", configurationModel.getModLoader());
    Assertions.assertEquals("36.1.2", configurationModel.getModLoaderVersion());
  }

  @Test
  void checkConfigurationNoFileTest() {
    ConfigurationModel configurationModel = new ConfigurationModel();
    List<String> clientMods =
        new ArrayList<>(
            Arrays.asList(
                "AmbientSounds",
                "BackTools",
                "BetterAdvancement",
                "BetterPing",
                "cherished",
                "ClientTweaks",
                "Controlling",
                "DefaultOptions",
                "durability",
                "DynamicSurroundings",
                "itemzoom",
                "jei-professions",
                "jeiintegration",
                "JustEnoughResources",
                "MouseTweaks",
                "Neat",
                "OldJavaWarning",
                "PackMenu",
                "preciseblockplacing",
                "SimpleDiscordRichPresence",
                "SpawnerFix",
                "TipTheScales",
                "WorldNameRandomizer"));
    List<String> copyDirs =
        new ArrayList<>(Arrays.asList("config", "mods", "scripts", "seeds", "defaultconfigs"));
    configurationModel.setModpackDir("./backend/test/resources/forge_tests");
    configurationModel.setClientMods(clientMods);
    configurationModel.setCopyDirs(copyDirs);
    configurationModel.setIncludeServerInstallation(true);
    configurationModel.setIncludeServerIcon(true);
    configurationModel.setIncludeServerProperties(true);
    configurationModel.setIncludeZipCreation(true);
    configurationModel.setModLoader("Forge");
    configurationModel.setModLoaderVersion("36.1.2");
    configurationModel.setMinecraftVersion("1.16.5");
    Assertions.assertFalse(
        configurationHandler.checkConfiguration(configurationModel, new ArrayList<>(), true));
    Assertions.assertEquals(
        "./backend/test/resources/forge_tests", configurationModel.getModpackDir());
    Assertions.assertEquals("1.16.5", configurationModel.getMinecraftVersion());
    Assertions.assertEquals("Forge", configurationModel.getModLoader());
    Assertions.assertEquals("36.1.2", configurationModel.getModLoaderVersion());

    configurationModel.setModLoader("Fabric");
    configurationModel.setModLoaderVersion("0.14.6");
    Assertions.assertFalse(
        configurationHandler.checkConfiguration(configurationModel, new ArrayList<>(), true));
    Assertions.assertEquals("Fabric", configurationModel.getModLoader());
    Assertions.assertEquals("0.14.6", configurationModel.getModLoaderVersion());

    configurationModel.setModLoader("Quilt");
    configurationModel.setModLoaderVersion("0.16.1");
    Assertions.assertFalse(
        configurationHandler.checkConfiguration(configurationModel, new ArrayList<>(), true));
    Assertions.assertEquals("Quilt", configurationModel.getModLoader());
    Assertions.assertEquals("0.16.1", configurationModel.getModLoaderVersion());
  }

  @Test
  void checkIconAndPropertiesTest() {
    Assertions.assertTrue(configurationHandler.checkIconAndProperties(""));
    Assertions.assertFalse(configurationHandler.checkIconAndProperties("/some/path"));
    Assertions.assertTrue(configurationHandler.checkIconAndProperties("img/prosper.png"));
  }

  @Test
  void checkZipArchiveTest() {
    Assertions.assertFalse(
        configurationHandler.checkZipArchive(
            Paths.get("backend/test/resources/testresources/Survive_Create_Prosper_4_valid.zip"),
            new ArrayList<>()));

    Assertions.assertTrue(
        configurationHandler.checkZipArchive(
            Paths.get("backend/test/resources/testresources/Survive_Create_Prosper_4_invalid.zip"),
            new ArrayList<>()));
  }


  @Test
  void writeConfigToFileTestFabric() {
    List<String> clientMods =
        new ArrayList<>(
            Arrays.asList(
                "AmbientSounds",
                "BackTools",
                "BetterAdvancement",
                "BetterPing",
                "cherished",
                "ClientTweaks",
                "Controlling",
                "DefaultOptions",
                "durability",
                "DynamicSurroundings",
                "itemzoom",
                "jei-professions",
                "jeiintegration",
                "JustEnoughResources",
                "MouseTweaks",
                "Neat",
                "OldJavaWarning",
                "PackMenu",
                "preciseblockplacing",
                "SimpleDiscordRichPresence",
                "SpawnerFix",
                "TipTheScales",
                "WorldNameRandomizer"));
    List<String> copyDirs =
        new ArrayList<>(Arrays.asList("config", "mods", "scripts", "seeds", "defaultconfigs"));

    String javaPath;
    String autoJavaPath = System.getProperty("java.home").replace("\\", "/") + "/bin/java";

    if (autoJavaPath.startsWith("C:")) {
      autoJavaPath = String.format("%s.exe", autoJavaPath);
    }
    if (new File("/usr/bin/java").exists()) {
      javaPath = "/usr/bin/java";
    } else {
      javaPath = autoJavaPath;
    }

    String javaArgs = "tf3g4jz89agz843fag8z49a3zg8ap3jg8zap9vagv3z8j";

    Assertions.assertNotNull(
        new ConfigurationModel(
            clientMods,
            copyDirs,
            "./backend/test/resources/fabric_tests",
            javaPath,
            "1.16.5",
            "Fabric",
            "0.11.3",
            javaArgs,
            "",
            "",
            true,
            true,
            true,
            true,
            new HashMap<>(),
            new HashMap<>()
        ).save(new File("tests/serverpackcreatorfabric.conf")));
    Assertions.assertTrue(new File("tests/serverpackcreatorfabric.conf").exists());
  }

  @Test
  void writeConfigToFileTestForge() {
    List<String> clientMods =
        new ArrayList<>(
            Arrays.asList(
                "AmbientSounds",
                "BackTools",
                "BetterAdvancement",
                "BetterPing",
                "cherished",
                "ClientTweaks",
                "Controlling",
                "DefaultOptions",
                "durability",
                "DynamicSurroundings",
                "itemzoom",
                "jei-professions",
                "jeiintegration",
                "JustEnoughResources",
                "MouseTweaks",
                "Neat",
                "OldJavaWarning",
                "PackMenu",
                "preciseblockplacing",
                "SimpleDiscordRichPresence",
                "SpawnerFix",
                "TipTheScales",
                "WorldNameRandomizer"));
    List<String> copyDirs =
        new ArrayList<>(Arrays.asList("config", "mods", "scripts", "seeds", "defaultconfigs"));

    String javaPath;
    String autoJavaPath = System.getProperty("java.home").replace("\\", "/") + "/bin/java";

    if (autoJavaPath.startsWith("C:")) {
      autoJavaPath = String.format("%s.exe", autoJavaPath);
    }
    if (new File("/usr/bin/java").exists()) {
      javaPath = "/usr/bin/java";
    } else {
      javaPath = autoJavaPath;
    }

    String javaArgs = "tf3g4jz89agz843fag8z49a3zg8ap3jg8zap9vagv3z8j";

    Assertions.assertNotNull(
        new ConfigurationModel(
            clientMods,
            copyDirs,
            "./backend/test/resources/forge_tests",
            javaPath,
            "1.16.5",
            "Forge",
            "36.1.2",
            javaArgs,
            "",
            "",
            true,
            true,
            true,
            true,
            new HashMap<>(),
            new HashMap<>()
        ).save(new File("tests/serverpackcreatorforge.conf")));
    Assertions.assertTrue(new File("tests/serverpackcreatorforge.conf").exists());
  }

  @Test
  void writeConfigToFileModelTest() {
    ConfigurationModel configurationModel = new ConfigurationModel();
    configurationModel.setModpackDir("backend/test/resources/forge_tests");
    configurationModel.setClientMods(
        new ArrayList<>(
            Arrays.asList(
                "AmbientSounds",
                "BackTools",
                "BetterAdvancement",
                "BetterPing",
                "cherished",
                "ClientTweaks",
                "Controlling",
                "DefaultOptions",
                "durability",
                "DynamicSurroundings",
                "itemzoom",
                "jei-professions",
                "jeiintegration",
                "JustEnoughResources",
                "MouseTweaks",
                "Neat",
                "OldJavaWarning",
                "PackMenu",
                "preciseblockplacing",
                "SimpleDiscordRichPresence",
                "SpawnerFix",
                "TipTheScales",
                "WorldNameRandomizer")));
    configurationModel.setCopyDirs(
        new ArrayList<>(Arrays.asList("config", "mods", "scripts", "seeds", "defaultconfigs")));
    configurationModel.setIncludeServerInstallation(true);
    configurationModel.setIncludeServerIcon(true);
    configurationModel.setIncludeServerProperties(true);
    configurationModel.setIncludeZipCreation(true);
    configurationModel.setMinecraftVersion("1.16.5");
    configurationModel.setModLoader("Forge");
    configurationModel.setModLoaderVersion("36.1.2");
    configurationModel.setJavaArgs("tf3g4jz89agz843fag8z49a3zg8ap3jg8zap9vagv3z8j");
    Assertions.assertNotNull(configurationModel.save(new File("tests/somefile.conf")));
    Assertions.assertTrue(new File("tests/somefile.conf").exists());
  }

  @Test
  void setModLoaderCaseTestForge() {
    Assertions.assertEquals("Forge", configurationHandler.getModLoaderCase("fOrGe"));
  }

  @Test
  void setModLoaderCaseTestFabric() {
    Assertions.assertEquals("Fabric", configurationHandler.getModLoaderCase("fAbRiC"));
  }

  @Test
  void setModLoaderCaseTestForgeCorrected() {
    Assertions.assertEquals("Forge", configurationHandler.getModLoaderCase("eeeeefOrGeeeeee"));
  }

  @Test
  void setModLoaderCaseTestFabricCorrected() {
    Assertions.assertEquals("Fabric",
                            configurationHandler.getModLoaderCase("hufwhafasfabricfagrsg"));
  }

  @Test
  void printConfigModelTest() {
    ConfigurationModel configurationModel = new ConfigurationModel();
    configurationModel.setModpackDir("backend/test/resources/forge_tests");
    configurationModel.setClientMods(
        new ArrayList<>(
            Arrays.asList(
                "AmbientSounds",
                "BackTools",
                "BetterAdvancement",
                "BetterPing",
                "cherished",
                "ClientTweaks",
                "Controlling",
                "DefaultOptions",
                "durability",
                "DynamicSurroundings",
                "itemzoom",
                "jei-professions",
                "jeiintegration",
                "JustEnoughResources",
                "MouseTweaks",
                "Neat",
                "OldJavaWarning",
                "PackMenu",
                "preciseblockplacing",
                "SimpleDiscordRichPresence",
                "SpawnerFix",
                "TipTheScales",
                "WorldNameRandomizer")));
    configurationModel.setCopyDirs(
        new ArrayList<>(Arrays.asList("config", "mods", "scripts", "seeds", "defaultconfigs")));
    configurationModel.setIncludeServerInstallation(true);
    configurationModel.setIncludeServerIcon(true);
    configurationModel.setIncludeServerProperties(true);
    configurationModel.setIncludeZipCreation(true);
    configurationModel.setMinecraftVersion("1.16.5");
    configurationModel.setModLoader("Forge");
    configurationModel.setModLoaderVersion("36.1.2");
    configurationModel.setJavaArgs("tf3g4jz89agz843fag8z49a3zg8ap3jg8zap9vagv3z8j");
    configurationHandler.printConfigurationModel(configurationModel);
  }

  @Test
  void updateConfigModelFromModrinthManifestTest() throws IOException {
    ConfigurationModel configurationModel = new ConfigurationModel();
    configurationHandler.updateConfigModelFromModrinthManifest(
        configurationModel,
        new File("backend/test/resources/testresources/modrinth/forge_modrinth.index.json"));
    Assertions.assertEquals(configurationModel.getMinecraftVersion(), "1.18.2");
    Assertions.assertEquals(configurationModel.getModLoader(), "Forge");
    Assertions.assertEquals(configurationModel.getModLoaderVersion(), "40.1.48");

    configurationHandler.updateConfigModelFromModrinthManifest(
        configurationModel,
        new File("backend/test/resources/testresources/modrinth/fabric_modrinth.index.json"));
    Assertions.assertEquals(configurationModel.getMinecraftVersion(), "1.19");
    Assertions.assertEquals(configurationModel.getModLoader(), "Fabric");
    Assertions.assertEquals(configurationModel.getModLoaderVersion(), "0.14.8");

    configurationHandler.updateConfigModelFromModrinthManifest(
        configurationModel,
        new File("backend/test/resources/testresources/modrinth/quilt_modrinth.index.json"));
    Assertions.assertEquals(configurationModel.getMinecraftVersion(), "1.19");
    Assertions.assertEquals(configurationModel.getModLoader(), "Quilt");
    Assertions.assertEquals(configurationModel.getModLoaderVersion(), "0.17.0");
  }

  @Test
  void updateConfigModelFromCurseManifestTest() throws IOException {
    ConfigurationModel configurationModel = new ConfigurationModel();
    configurationHandler.updateConfigModelFromCurseManifest(
        configurationModel,
        new File("backend/test/resources/testresources/curseforge/forge_manifest.json"));
    Assertions.assertEquals(configurationModel.getMinecraftVersion(), "1.16.5");
    Assertions.assertEquals(configurationModel.getModLoader(), "Forge");
    Assertions.assertEquals(configurationModel.getModLoaderVersion(), "36.0.1");

    configurationHandler.updateConfigModelFromCurseManifest(
        configurationModel,
        new File("backend/test/resources/testresources/curseforge/fabric_manifest.json"));
    Assertions.assertEquals(configurationModel.getMinecraftVersion(), "1.18.2");
    Assertions.assertEquals(configurationModel.getModLoader(), "Fabric");
    Assertions.assertEquals(configurationModel.getModLoaderVersion(), "0.13.3");
  }

  @Test
  void updateConfigModelFromMinecraftInstanceTest() throws IOException {
    ConfigurationModel configurationModel = new ConfigurationModel();
    configurationHandler.updateConfigModelFromMinecraftInstance(
        configurationModel,
        new File("backend/test/resources/testresources/curseforge/forge_minecraftinstance.json"));
    Assertions.assertEquals(configurationModel.getMinecraftVersion(), "1.16.5");
    Assertions.assertEquals(configurationModel.getModLoader(), "Forge");
    Assertions.assertEquals(configurationModel.getModLoaderVersion(), "36.2.4");

    configurationHandler.updateConfigModelFromMinecraftInstance(
        configurationModel,
        new File("backend/test/resources/testresources/curseforge/fabric_minecraftinstance.json"));
    Assertions.assertEquals(configurationModel.getMinecraftVersion(), "1.18.2");
    Assertions.assertEquals(configurationModel.getModLoader(), "Fabric");
    Assertions.assertEquals(configurationModel.getModLoaderVersion(), "0.13.3");
  }

  @Test
  void updateConfigModelFromConfigJsonTest() throws IOException {
    ConfigurationModel configurationModel = new ConfigurationModel();
    configurationHandler.updateConfigModelFromConfigJson(
        configurationModel,
        new File("backend/test/resources/testresources/gdlauncher/fabric_config.json"));
    Assertions.assertEquals(configurationModel.getMinecraftVersion(), "1.18.2");
    Assertions.assertEquals(configurationModel.getModLoader(), "Fabric");
    Assertions.assertEquals(configurationModel.getModLoaderVersion(), "0.14.8");

    configurationHandler.updateConfigModelFromConfigJson(
        configurationModel,
        new File("backend/test/resources/testresources/gdlauncher/forge_config.json"));
    Assertions.assertEquals(configurationModel.getMinecraftVersion(), "1.18.2");
    Assertions.assertEquals(configurationModel.getModLoader(), "Forge");
    Assertions.assertEquals(configurationModel.getModLoaderVersion(), "40.1.52");
  }

  @Test
  void updateConfigModelFromMMCPackTest() throws IOException {
    ConfigurationModel configurationModel = new ConfigurationModel();
    configurationHandler.updateConfigModelFromMMCPack(
        configurationModel,
        new File("backend/test/resources/testresources/multimc/fabric_mmc-pack.json"));
    Assertions.assertEquals(configurationModel.getMinecraftVersion(), "1.18.1");
    Assertions.assertEquals(configurationModel.getModLoader(), "Fabric");
    Assertions.assertEquals(configurationModel.getModLoaderVersion(), "0.12.12");

    configurationHandler.updateConfigModelFromMMCPack(
        configurationModel,
        new File("backend/test/resources/testresources/multimc/forge_mmc-pack.json"));
    Assertions.assertEquals(configurationModel.getMinecraftVersion(), "1.16.5");
    Assertions.assertEquals(configurationModel.getModLoader(), "Forge");
    Assertions.assertEquals(configurationModel.getModLoaderVersion(), "36.2.23");

    configurationHandler.updateConfigModelFromMMCPack(
        configurationModel,
        new File("backend/test/resources/testresources/multimc/quilt_mmc-pack.json"));
    Assertions.assertEquals(configurationModel.getMinecraftVersion(), "1.19");
    Assertions.assertEquals(configurationModel.getModLoader(), "Quilt");
    Assertions.assertEquals(configurationModel.getModLoaderVersion(), "0.17.0");
  }

  @Test
  void updateDestinationFromInstanceCfgTest() throws IOException {
    Assertions.assertEquals(
        configurationHandler.updateDestinationFromInstanceCfg(
            new File("backend/test/resources/testresources/multimc/better_mc_instance.cfg")),
        "Better Minecraft [FABRIC] - 1.18.1");

    Assertions.assertEquals(
        configurationHandler.updateDestinationFromInstanceCfg(
            new File("backend/test/resources/testresources/multimc/all_the_mods_instance.cfg")),
        "All the Mods 6 - ATM6 - 1.16.5");
  }

  @Test
  void suggestCopyDirsTest() {
    List<String> dirs = configurationHandler.suggestCopyDirs("backend/test/resources/fabric_tests");
    Assertions.assertTrue(dirs.contains("config"));
    Assertions.assertTrue(dirs.contains("defaultconfigs"));
    Assertions.assertTrue(dirs.contains("mods"));
    Assertions.assertTrue(dirs.contains("scripts"));
    Assertions.assertTrue(dirs.contains("seeds"));
    Assertions.assertFalse(dirs.contains("server_pack"));
  }

  @Test
  void directoriesInModpackZipTest() throws IOException {
    List<String> entries =
        configurationHandler.getDirectoriesInModpackZipBaseDirectory(
            new ZipFile(
                "backend/test/resources/testresources/Survive_Create_Prosper_4_invalid.zip"));
    Assertions.assertEquals(1, entries.size());
    Assertions.assertTrue(entries.contains("overrides/"));
    entries =
        configurationHandler.getDirectoriesInModpackZipBaseDirectory(
            new ZipFile("backend/test/resources/testresources/Survive_Create_Prosper_4_valid.zip"));
    Assertions.assertTrue(entries.size() > 1);
    Assertions.assertTrue(entries.contains("mods/"));
    Assertions.assertTrue(entries.contains("config/"));
  }

  @Test
  void filesAndDirsInZipTest() throws IOException {
    Assertions.assertTrue(
        configurationHandler
            .getFilesInModpackZip(
                new ZipFile(
                    "backend/test/resources/testresources/Survive_Create_Prosper_4_invalid.zip"))
            .size()
            > 0);
    Assertions.assertTrue(
        configurationHandler
            .getFilesInModpackZip(
                new ZipFile(
                    "backend/test/resources/testresources/Survive_Create_Prosper_4_valid.zip"))
            .size()
            > 0);

    Assertions.assertTrue(
        configurationHandler
            .getDirectoriesInModpackZip(
                new ZipFile(
                    "backend/test/resources/testresources/Survive_Create_Prosper_4_invalid.zip"))
            .size()
            > 0);
    Assertions.assertTrue(
        configurationHandler
            .getDirectoriesInModpackZip(
                new ZipFile(
                    "backend/test/resources/testresources/Survive_Create_Prosper_4_valid.zip"))
            .size()
            > 0);

    Assertions.assertTrue(
        configurationHandler
            .getAllFilesAndDirectoriesInModpackZip(
                new ZipFile(
                    "backend/test/resources/testresources/Survive_Create_Prosper_4_invalid.zip"))
            .size()
            > 0);
    Assertions.assertTrue(
        configurationHandler
            .getAllFilesAndDirectoriesInModpackZip(
                new ZipFile(
                    "backend/test/resources/testresources/Survive_Create_Prosper_4_valid.zip"))
            .size()
            > 0);
  }
}
