/* Copyright (C) 2022  Griefed
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * The full license can be found at https:github.com/Griefed/ServerPackCreator/blob/main/LICENSE
 */
/**
 * The utilities package holds classes and methods for various useful bits and pieces used all
 * throughout ServerPackCreator. If any given method is helpful in multiple steps throughout the
 * ServerPackCreator architecture, it usually belongs to one of the utility classes. <br>
 * <br>
 *
 * <p>Such utilities include, but are not limited to converting Strings to booleans, encapsulating
 * each entry in a String List with quotation-marks, downloading files, copying files and folders
 * from a JAR-file and so on and so forth.<br>
 * <br>
 *
 * <p>Basically, whenever a method in any SeverPackCreator class becomes useful to multiple
 * classes, it is usually moved into any already existing utility-class or a new one is added if no
 * already-fitting one exists.
 *
 * @author Griefed
 */
package de.griefed.serverpackcreator.utilities;
