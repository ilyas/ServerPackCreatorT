/* Copyright (C) 2022  Griefed
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * The full license can be found at https:github.com/Griefed/ServerPackCreator/blob/main/LICENSE
 */
package de.griefed.serverpackcreator.utilities.common;

import java.awt.Desktop;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import mslinks.ShellLink;
import mslinks.ShellLinkException;
import net.lingala.zip4j.ZipFile;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

/**
 * Utility-class revolving around various file-interactions.
 *
 * @author Griefed
 */
@Component
public final class FileUtilities {

  private static final Logger LOG = LogManager.getLogger(FileUtilities.class);

  @Contract(pure = true)
  public FileUtilities() {
  }

  /**
   * Move a file from source to destination, and replace the destination file if it exists.
   *
   * @param sourceFile      The source file.
   * @param destinationFile The destination file to be replaced by the source file.
   * @return  Returns true if the file was sucessfully replaced.
   * @throws IOException Thrown if an error occurs when the file is moved.
   * @author Griefed
   */
  public boolean replaceFile(@NotNull File sourceFile,
                             @NotNull File destinationFile) throws IOException {

    if (sourceFile.exists() && destinationFile.delete()) {

      FileUtils.moveFile(sourceFile, destinationFile);
      return true;
    }

    LOG.error("Source file not found.");

    return false;
  }

  /**
   * Create a directory, including any necessary parent directories. If an error is encountered, it
   * is printed to our logs.
   *
   * @param directory The directory to create.
   * @author Griefed
   */
  public void createDirectories(@NotNull Path directory) {
    try {
      Files.createDirectories(directory);
    } catch (IOException ex) {
      LOG.error("Could not create directory: " + directory, ex);
    }
  }

  /**
   * Unzips the downloaded modpack ZIP-archive to the specified directory.
   *
   * @param zipFile              The path to the ZIP-archive which we want to unzip.
   * @param destinationDirectory The directory into which the ZIP-archive will be unzipped into.
   * @author Griefed
   */
  public void unzipArchive(@NotNull String zipFile,
                           @NotNull String destinationDirectory) {
    LOG.info("Extracting ZIP-file: " + zipFile);

    try (ZipFile zip = new ZipFile(zipFile)) {

      zip.extractAll(destinationDirectory);

    } catch (IOException ex) {

      LOG.error("Error: There was an error extracting the archive " + zipFile, ex);
    }
  }

  /**
   * Check the given file for its type, whether it is a regular file, a Windows link or a UNIX
   * symlink.
   *
   * @param file The file to check
   * @return The type of the given file. Either {@link FileType#FILE}, {@link FileType#LINK} or
   * {@link FileType#SYMLINK}
   * @author Griefed
   */
  public @NotNull FileType checkFileType(@NotNull String file) {
    if (file.isEmpty()) {
      return FileType.INVALID;
    }
    return checkFileType(new File(file));
  }

  /**
   * Check the given file for its type, whether it is a regular file, a Windows link or a UNIX
   * symlink.
   *
   * @param file The file to check
   * @return The type of the given file. Either {@link FileType#FILE}, {@link FileType#LINK} or
   * {@link FileType#SYMLINK}
   * @author Griefed
   */
  public @NotNull FileType checkFileType(@NotNull File file) {
    if (file.getName().endsWith("lnk")) {
      return FileType.LINK;
    }

    if (file.isDirectory()) {
      return FileType.DIRECTORY;
    }

    if (FileUtils.isSymlink(file)) {
      return FileType.SYMLINK;
    }

    if (file.isFile()) {
      return FileType.FILE;
    }

    return FileType.INVALID;
  }

  /**
   * Check if the given file is a UNIX symlink or Windows lnk.
   *
   * @param file The file to check.
   * @return {@code true} if the given file is a UNIX symlink or Windows lnk.
   * @author Griefed
   */
  public boolean isLink(@NotNull String file) {
    return isLink(new File(file));
  }

  /**
   * Check if the given file is a UNIX symlink or Windows lnk.
   *
   * @param file The file to check.
   * @return {@code true} if the given file is a UNIX symlink or Windows lnk.
   * @author Griefed
   */
  public boolean isLink(@NotNull File file) {
    if (file.getName().endsWith("lnk")) {
      return true;
    }

    return !file.toString().matches("[A-Za-z]:.*") && FileUtils.isSymlink(file);
  }

  /**
   * Resolve a given link/symlink to its source.
   *
   * @param link The link you want to resolve.
   * @return Path to the source of the link. If the specified file is not a link, the path to the
   * passed file is returned.
   * @throws IOException              if the link could not be parsed.
   * @throws InvalidFileTypeException if the specified file is neither a file, lnk nor symlink.
   * @author Griefed
   */
  public @NotNull String resolveLink(@NotNull String link)
      throws InvalidFileTypeException, IOException {
    return resolveLink(new File(link));
  }

  /**
   * Resolve a given link/symlink to its source.
   *
   * @param link The link you want to resolve.
   * @return Path to the source of the link. If the specified file is not a link, the path to the
   * passed file is returned.
   * @throws IOException              if the link could not be parsed.
   * @throws InvalidFileTypeException if the specified file is neither a file, lnk nor symlink.
   * @author Griefed
   */
  public @NotNull String resolveLink(@NotNull File link)
      throws IOException, InvalidFileTypeException {

    FileType type = checkFileType(link);

    switch (type) {
      case LINK:
      case SYMLINK:
        try {
          return resolveLink(link, type);
        } catch (InvalidFileTypeException | InvalidLinkException | ShellLinkException ex) {
          LOG.error("Somehow an invalid FileType was specified. Please report this on GitHub!", ex);
        }

      case FILE:
      case DIRECTORY:
        return link.toString();

      case INVALID:
      default:
        throw new InvalidFileTypeException("FileType must be either LINK or SYMLINK");
    }
  }

  /**
   * Resolve a given link/symlink to its source.<br> This would not exist without the great answers
   * from this StackOverflow question: <a
   * href="https://stackoverflow.com/questions/309495/windows-shortcut-lnk-parser-in-java">Windows
   * Shortcut lnk parser in Java</a><br> Huge shoutout to <a
   * href="https://stackoverflow.com/users/675721/codebling">Codebling</a>
   *
   * @param file     The file of which to acquire the source.
   * @param fileType The link-type. Either {@link FileType#LINK} for Windows, or
   *                 {@link FileType#SYMLINK} for UNIX systems.
   * @return The path to the source of the given link.
   * @throws InvalidFileTypeException if the specified {@link FileType} is invalid.
   * @throws InvalidLinkException     if the specified file is not a valid Windows link.
   * @throws ShellLinkException       if the specified file could not be parsed as a Windows link.
   * @throws IOException              if the link could not be parsed.
   * @author Griefed
   */
  private @NotNull String resolveLink(@NotNull File file,
                                      @NotNull FileType fileType)
      throws InvalidFileTypeException, IOException, InvalidLinkException, ShellLinkException {
    switch (fileType) {
      case SYMLINK:
        return file.getPath();

      case LINK:
        return new ShellLink(file).resolveTarget();

      default:
        throw new InvalidFileTypeException("FileType must be either LINK or SYMLINK");
    }
  }

  /**
   * Check the given file or directory for read- and write-permission.
   *
   * @param fileOrDirectory File or directory.
   * @return {@code true} if both read- and write-permissions are set.
   * @throws InvalidPathException if the path string cannot be converted to a Path.
   * @author Griefed
   */
  public boolean isReadWritePermissionSet(@NotNull String fileOrDirectory) throws InvalidPathException {
    return isReadWritePermissionSet(Paths.get(fileOrDirectory));
  }

  /**
   * Check the given file or directory for read- and write-permission.
   *
   * @param fileOrDirectory File or directory.
   * @return {@code true} if both read- and write-permissions are set.
   * @author Griefed
   */
  public boolean isReadWritePermissionSet(@NotNull Path fileOrDirectory) {
    return isReadPermissionSet(fileOrDirectory) && isWritePermissionSet(fileOrDirectory);
  }

  /**
   * Check the given file or directory for read-permission.
   *
   * @param fileOrDirectory File or directory.
   * @return {@code true} if read-permissions are set.
   * @author Griefed
   */
  public boolean isReadPermissionSet(@NotNull Path fileOrDirectory) {

    try {
      if (!Files.isReadable(fileOrDirectory)) {

        LOG.error(String.format("No read-permission for %s", fileOrDirectory));
        return false;
      }
    } catch (SecurityException ex) {

      LOG.error(String.format("No read-permission for %s", fileOrDirectory), ex);
      return false;
    }

    return true;
  }

  /**
   * Check the given file or directory for write-permission.
   *
   * @param fileOrDirectory File or directory.
   * @return {@code true} if write-permissions are set.
   * @author Griefed
   */
  public boolean isWritePermissionSet(@NotNull Path fileOrDirectory) {

    try {
      if (!Files.isWritable(fileOrDirectory)) {

        LOG.error(String.format("No write-permission for %s", fileOrDirectory));
        return false;
      }
    } catch (SecurityException ex) {

      LOG.error(String.format("No write-permission for %s", fileOrDirectory), ex);
      return false;
    }

    return true;
  }

  /**
   * Check the given file or directory for read- and write-permission.
   *
   * @param fileOrDirectory File or directory.
   * @return {@code true} if both read- and write-permissions are set.
   * @throws InvalidPathException if a {@code Path} object cannot be constructed from the abstract
   *                              path (see
   *                              {@link java.nio.file.FileSystem#getPath FileSystem.getPath})
   * @author Griefed
   */
  public boolean isReadWritePermissionSet(@NotNull File fileOrDirectory) throws InvalidPathException {
    return isReadWritePermissionSet(fileOrDirectory.toPath());
  }

  /**
   * Check the given file or directory for read-permission.
   *
   * @param fileOrDirectory File or directory.
   * @return {@code true} if read-permissions are set.
   * @author Griefed
   */
  public boolean isReadPermissionSet(@NotNull String fileOrDirectory) {
    return isReadPermissionSet(Paths.get(fileOrDirectory));
  }

  /**
   * Check the given file or directory for read-permission.
   *
   * @param fileOrDirectory File or directory.
   * @return {@code true} if read-permissions are set.
   * @author Griefed
   */
  public boolean isReadPermissionSet(@NotNull File fileOrDirectory) {
    return isReadPermissionSet(fileOrDirectory.toPath());
  }

  /**
   * Check the given file or directory for write-permission.
   *
   * @param fileOrDirectory File or directory.
   * @return {@code true} if write-permissions are set.
   * @author Griefed
   */
  public boolean isWritePermissionSet(@NotNull String fileOrDirectory) {
    return isReadPermissionSet(Paths.get(fileOrDirectory));
  }

  /**
   * Check the given file or directory for write-permission.
   *
   * @param fileOrDirectory File or directory.
   * @return {@code true} if write-permissions are set.
   * @author Griefed
   */
  public boolean isWritePermissionSet(@NotNull File fileOrDirectory) {
    return isReadPermissionSet(fileOrDirectory.toPath());
  }

  /**
   * Open the specified folder in the file explorer.
   *
   * @param folder The folder to open.
   * @author Griefed
   */
  public void openFolder(@NotNull String folder) {
    openFolder(new File(folder));
  }

  /**
   * Open the specified folder in the file explorer.
   *
   * @param folder The folder to open.
   * @author Griefed
   */
  public void openFolder(@NotNull File folder) {
    if (GraphicsEnvironment.isHeadless()) {
      LOG.error("Graphics environment not supported.");
    } else {
      if (Desktop.getDesktop().isSupported(Desktop.Action.OPEN)) {
        try {
          Desktop.getDesktop().open(folder);
        } catch (IOException ex) {
          LOG.error("Error opening file explorer for " + folder + ".", ex);
        }
      }
    }
  }

  /**
   * Open the specified file in an editor.
   *
   * @param fileToOpen The file to open.
   * @author Griefed
   */
  public void openFile(@NotNull String fileToOpen) {
    openFile(new File(fileToOpen));
  }

  /**
   * Open the specified file in an editor.
   *
   * @param fileToOpen The file to open.
   * @author Griefed
   */
  public void openFile(@NotNull File fileToOpen) {
    if (GraphicsEnvironment.isHeadless()) {
      LOG.error("Graphics environment not supported.");
    } else {
      try {
        if (Desktop.getDesktop().isSupported(Desktop.Action.EDIT)) {
          Desktop.getDesktop().open(fileToOpen);
        }
      } catch (IOException ex) {
        LOG.error("Error opening file.", ex);
      }
    }
  }

  public enum FileType {

    /**
     * A regular file.
     */
    FILE,

    /**
     * A regular directory.
     */
    DIRECTORY,

    /**
     * A Windows link.
     */
    LINK,

    /**
     * A UNIX symlink.
     */
    SYMLINK,

    /**
     * Not a valid file.
     */
    INVALID
  }
}
