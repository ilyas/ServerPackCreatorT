/* Copyright (C) 2022  Griefed
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * The full license can be found at https:github.com/Griefed/ServerPackCreator/blob/main/LICENSE
 */
package de.griefed.serverpackcreator.spring.task;

import java.io.Serializable;

/**
 * <a
 * href="https://dev.to/gotson/how-to-implement-a-task-queue-using-apache-artemis-and-spring-boot-2mme">How
 * to implement a task queue using Apache Artemis and Spring Boot</a><br> Huge Thank You to <a
 * href="https://github.com/gotson">Gauthier</a> for writing the above guide on how to implement a
 * JMS. Without it this implementation of Artemis would have either taken way longer or never
 * happened at all. I managed to translate their Kotlin-code to Java and make the necessary changes
 * to fully implement it in ServerPackCreator.<br>
 *
 * @author Griefed
 */
public abstract class Task implements Serializable {

  public abstract String uniqueId();
}
