/**
 * Everything revolving around server packs in the webservice. The model for database interaction,
 * REST endpoints for listing available server packs, requesting downloads etc.
 *
 * @author Griefed
 */
package de.griefed.serverpackcreator.spring.serverpack;
