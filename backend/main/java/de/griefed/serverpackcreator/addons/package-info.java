/* Copyright (C) 2022  Griefed
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * The full license can be found at https:github.com/Griefed/ServerPackCreator/blob/main/LICENSE
 */
/**
 * Annotations for ServerPackCreator plugins/addons. If you plan on adding additional entrypoints
 * for addons and/or plugins, make sure to extend from
 * {@link de.griefed.serverpackcreator.addons.BaseInformation} and put your new annotation into a
 * sub-package corresponding to the part of ServerPackCreator where it will hook into.
 *
 * @author Griefed
 */
package de.griefed.serverpackcreator.addons;
