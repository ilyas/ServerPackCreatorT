/* Copyright (C) 2022  Griefed
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * The full license can be found at https:github.com/Griefed/ServerPackCreator/blob/main/LICENSE
 */
package de.griefed.serverpackcreator;

import com.electronwill.nightconfig.core.CommentedConfig;
import com.electronwill.nightconfig.core.Config;
import com.electronwill.nightconfig.core.file.FileConfig;
import com.electronwill.nightconfig.core.file.NoFormatFoundException;
import com.electronwill.nightconfig.core.io.WritingMode;
import com.electronwill.nightconfig.toml.TomlFormat;
import com.fasterxml.jackson.databind.JsonNode;
import de.griefed.serverpackcreator.utilities.common.Utilities;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;

/**
 * A ConfigurationModel contains the settings required by
 * {@link ServerPackHandler#run(ConfigurationModel)} to create a server pack. A configuration model
 * usually consists of:
 * <ul>
 *   <li>Modpack directory</li>
 *   <li>Minecraft version</li>
 *   <li>Modloader</li>
 *   <li>Modloader version</li>
 *   <li>Java args for the start scripts</li>
 *   <li>Files and directories to copy to the server pack</li>
 *   <li>Whether to pre-install the modloader server</li>
 *   <li>Whether to include a server-icon</li>
 *   <li>Whether to include a server.properties</li>
 *   <li>Whether to create a ZIP-archive</li>
 * </ul>
 *
 * @author Griefed
 */
public class ConfigurationModel {

  private final List<String> clientMods = new ArrayList<>(1000);
  private final List<String> copyDirs = new ArrayList<>(100);
  private final HashMap<String, String> scriptSettings = new HashMap<>(100);
  private final HashMap<String, ArrayList<CommentedConfig>> addonsConfigs = new HashMap<>(20);
  private String modpackDir = "";
  private String minecraftVersion = "";
  private String modLoader = "";
  private String modLoaderVersion = "";
  private String javaArgs = "";
  private String serverPackSuffix = "";
  private String serverIconPath = "";
  private String serverPropertiesPath = "";
  private Boolean includeServerInstallation = true;
  private Boolean includeServerIcon = true;
  private Boolean includeServerProperties = true;
  private Boolean includeZipCreation = true;
  private JsonNode modpackJson = null;
  private String projectName;
  private String fileName;
  private String fileDiskName;

  /**
   * Create an empty configuration model. Customize it before using it!
   *
   * @author Griefed
   */
  public ConfigurationModel() {
  }

  /**
   * Construct a new configuration model with custom values.
   *
   * @param clientMods                List of clientside mods to exclude from the server pack.
   * @param copyDirs                  List of directories and/or files to include in the server
   *                                  pack.
   * @param modpackDir                The path to the modpack.
   * @param minecraftVersion          The Minecraft version the modpack uses.
   * @param modLoader                 The modloader the modpack uses. Either {@code Forge},
   *                                  {@code Fabric} or {@code Quilt}.
   * @param modLoaderVersion          The modloader version the modpack uses.
   * @param javaArgs                  JVM flags to create the start scripts with.
   * @param serverPackSuffix          Suffix to create the server pack with.
   * @param serverIconPath            Path to the icon to use in the server pack.
   * @param serverPropertiesPath      Path to the server.properties to create the server pack with.
   * @param includeServerInstallation Whether to install the modloader server in the server pack.
   * @param includeServerIcon         Whether to include the server-icon.png in the server pack.
   * @param includeServerProperties   Whether to include the server.properties in the server pack.
   * @param includeZipCreation        Whether to create a ZIP-archive of the server pack.
   * @param scriptSettings            Map containing key-value pairs to be used in start script
   *                                  creation.
   * @param addonsConfigs             Configuration for any and all addons used by this
   *                                  configuration.
   * @author Griefed
   */
  public ConfigurationModel(
      @NotNull List<String> clientMods,
      @NotNull List<String> copyDirs,
      @NotNull String modpackDir,
      @NotNull String minecraftVersion,
      @NotNull String modLoader,
      @NotNull String modLoaderVersion,
      @NotNull String javaArgs,
      @NotNull String serverPackSuffix,
      @NotNull String serverIconPath,
      @NotNull String serverPropertiesPath,
      boolean includeServerInstallation,
      boolean includeServerIcon,
      boolean includeServerProperties,
      boolean includeZipCreation,
      @NotNull HashMap<String, String> scriptSettings,
      @NotNull HashMap<String, ArrayList<CommentedConfig>> addonsConfigs) {

    this.clientMods.addAll(clientMods);
    this.copyDirs.addAll(copyDirs);
    this.modpackDir = modpackDir;
    this.minecraftVersion = minecraftVersion;
    this.modLoader = modLoader;
    this.modLoaderVersion = modLoaderVersion;
    this.javaArgs = javaArgs;
    this.serverPackSuffix = serverPackSuffix;
    this.serverIconPath = serverIconPath;
    this.serverPropertiesPath = serverPropertiesPath;
    this.includeServerInstallation = includeServerInstallation;
    this.includeServerIcon = includeServerIcon;
    this.includeServerProperties = includeServerProperties;
    this.includeZipCreation = includeZipCreation;
    this.scriptSettings.putAll(scriptSettings);
    this.addonsConfigs.putAll(addonsConfigs);
  }

  /**
   * Create a new configuration model from a config file.
   *
   * @param utilities  Instance of our SPC utilities.
   * @param configFile Configuration file to load.
   * @throws FileNotFoundException  if the specified file can not be found.
   * @throws NoFormatFoundException if the configuration format could not be determined by
   *                                Night-Config.
   * @author Griefed
   */
  public ConfigurationModel(@NotNull Utilities utilities,
                            @NotNull File configFile)
      throws FileNotFoundException, NoFormatFoundException {

    if (!configFile.exists()) {
      throw new FileNotFoundException("Couldn't find file: " + configFile);
    }

    FileConfig config = FileConfig.of(configFile, TomlFormat.instance());

    config.load();

    setClientMods(config.getOrElse("clientMods", Collections.singletonList("")));
    setCopyDirs(config.getOrElse("copyDirs", Collections.singletonList("")));
    modpackDir = config.getOrElse("modpackDir", "");
    minecraftVersion = config.getOrElse("minecraftVersion", "");
    modLoader = config.getOrElse("modLoader", "");
    modLoaderVersion = config.getOrElse("modLoaderVersion", "");
    javaArgs = config.getOrElse("javaArgs", "");
    serverPackSuffix = utilities.StringUtils()
                                .pathSecureText(config.getOrElse("serverPackSuffix", ""));
    serverIconPath = config.getOrElse("serverIconPath", "");
    serverPropertiesPath = config.getOrElse("serverPropertiesPath", "");

    includeServerInstallation = config.getOrElse("includeServerInstallation", false);
    includeServerIcon = config.getOrElse("includeServerIcon", false);
    includeServerProperties = config.getOrElse("includeServerProperties", false);
    includeZipCreation = config.getOrElse("includeZipCreation", false);

    try {
      for (Map.Entry<String, Object> entry : ((CommentedConfig) config.get("addons")).valueMap()
                                                                                     .entrySet()) {
        addonsConfigs.put(entry.getKey(), (ArrayList<CommentedConfig>) entry.getValue());
      }
    } catch (Exception ignored) {

    }

    try {
      for (Map.Entry<String, Object> entry : ((CommentedConfig) config.get("scripts")).valueMap()
                                                                                      .entrySet()) {
        scriptSettings.put(entry.getKey(), entry.getValue().toString());
      }
    } catch (Exception ignored) {

    }

    if (!scriptSettings.containsKey("SPC_JAVA_SPC")) {
      scriptSettings.put("SPC_JAVA_SPC", "java");
    }

    config.close();
  }

  /**
   * Save this configuration.
   *
   * @param destination The file to store the configuration in.
   * @return The configuration for further operations.
   * @author Griefed
   */
  public ConfigurationModel save(@NotNull File destination) {

    CommentedConfig conf = TomlFormat.instance().createConfig();
    conf.set("includeServerInstallation",
             includeServerInstallation);
    conf.setComment("includeServerInstallation",
                    " Whether to install a Forge/Fabric/Quilt server for the serverpack. Must be true or false.\n Default value is true.");

    conf.setComment("serverIconPath",
                    "\n Path to a custom server-icon.png-file to include in the server pack.");
    conf.set("serverIconPath", serverIconPath);

    conf.setComment("copyDirs",
                    "\n Name of directories or files to include in serverpack.\n When specifying \"saves/world_name\", \"world_name\" will be copied to the base directory of the serverpack\n for immediate use with the server. Automatically set when projectID,fileID for modpackDir has been specified.\n Example: [config,mods,scripts]");
    conf.set("copyDirs", copyDirs);

    conf.setComment("serverPackSuffix",
                    "\n Suffix to append to the server pack to be generated. Can be left blank/empty.");
    conf.set("serverPackSuffix", serverPackSuffix);

    conf.setComment("clientMods",
                    "\n List of client-only mods to delete from serverpack.\n No need to include version specifics. Must be the filenames of the mods, not their project names on CurseForge!\n Example: [AmbientSounds-,ClientTweaks-,PackMenu-,BetterAdvancement-,jeiintegration-]");
    conf.set("clientMods", clientMods);

    conf.setComment("serverPropertiesPath",
                    "\n Path to a custom server.properties-file to include in the server pack.");
    conf.set("serverPropertiesPath", serverPropertiesPath);

    conf.setComment("includeServerProperties",
                    "\n Include a server.properties in your serverpack. Must be true or false.\n If no server.properties is provided but is set to true, a default one will be provided.\n Default value is true.");
    conf.set("includeServerProperties", includeServerProperties);

    conf.setComment("javaArgs",
                    "\n Java arguments to set in the start-scripts for the generated server pack. Default value is \"empty\".\n Leave as \"empty\" to not have Java arguments in your start-scripts.");
    conf.set("javaArgs", javaArgs);

    conf.setComment("modpackDir",
                    "\n Path to your modpack. Can be either relative or absolute.\n Example: \"./Some Modpack\" or \"C:/Minecraft/Some Modpack\"");
    conf.set("modpackDir", modpackDir);

    conf.setComment("includeServerIcon",
                    "\n Include a server-icon.png in your serverpack. Must be true or false\n Default value is true.");
    conf.set("includeServerIcon", includeServerIcon);

    conf.setComment("includeZipCreation",
                    "\n Create zip-archive of serverpack. Must be true or false.\n Default value is true.");
    conf.set("includeZipCreation", includeZipCreation);

    conf.setComment("modLoaderVersion",
                    "\n The version of the modloader you want to install. Example for Fabric=\"0.7.3\", example for Forge=\"36.0.15\".\n Automatically set when projectID,fileID for modpackDir has been specified.\n Only needed if includeServerInstallation is true.");
    conf.set("modLoaderVersion", modLoaderVersion);

    conf.setComment("minecraftVersion",
                    "\n Which Minecraft version to use. Example: \"1.16.5\".\n Automatically set when projectID,fileID for modpackDir has been specified.\n Only needed if includeServerInstallation is true.");
    conf.set("minecraftVersion", minecraftVersion);

    conf.setComment("modLoader",
                    "\n Which modloader to install. Must be either \"Forge\", \"Fabric\", \"Quilt\" or \"LegacyFabric\".\n Automatically set when projectID,fileID for modpackDir has been specified.\n Only needed if includeServerInstallation is true.");
    conf.set("modLoader", modLoader);

    Config addons = TomlFormat.newConfig();
    addons.valueMap().putAll(addonsConfigs);

    conf.setComment("addons",
                    "\n Configurations for any and all addons installed and used by this configuration.");
    conf.setComment("addons", " Settings related to addons. An addon is identified by its ID.");
    conf.set("addons", addons);

    Config scripts = TomlFormat.newConfig();
    for (Map.Entry<String, String> entry : scriptSettings.entrySet()) {
      if (!entry.getKey()
                .equals("SPC_SERVERPACKCREATOR_VERSION_SPC") && !entry.getKey()
                                                                      .equals(
                                                                          "SPC_MINECRAFT_VERSION_SPC")
          && !entry.getKey()
                   .equals("SPC_MODLOADER_SPC") && !entry.getKey()
                                                         .equals("SPC_MODLOADER_VERSION_SPC")
          && !entry.getKey()
                   .equals("SPC_JAVA_ARGS_SPC") && !entry.getKey()
                                                         .equals("SPC_FABRIC_INSTALLER_VERSION_SPC")
          && !entry.getKey()
                   .equals("SPC_QUILT_INSTALLER_VERSION_SPC") && !entry.getKey()
                                                                       .equals(
                                                                           "SPC_LEGACYFABRIC_INSTALLER_VERSION_SPC")
          && !entry.getKey()
                   .equals("SPC_MINECRAFT_SERVER_URL_SPC")) {
        scripts.set(entry.getKey(), entry.getValue());
      }
    }
    conf.setComment("scripts",
                    "\n Key-value pairs for start scripts. A given key in a start script is replaced with the value.");
    conf.add("scripts", scripts);

    TomlFormat.instance().createWriter()
              .write(conf, destination, WritingMode.REPLACE, StandardCharsets.UTF_8);

    return this;
  }

  /**
   * Get the configurations for various addons.
   *
   * @return A hashmap containing configurations for various addons.
   * @author Griefed
   */
  HashMap<String, ArrayList<CommentedConfig>> getAddonsConfigs() {
    return addonsConfigs;
  }

  /**
   * Clear and set the configs for addons.
   *
   * @param addonsConfigs The new configurations for various addons.
   * @author Griefed
   */
  void setAddonsConfigs(@NotNull HashMap<String, ArrayList<CommentedConfig>> addonsConfigs) {
    this.addonsConfigs.clear();
    this.addonsConfigs.putAll(addonsConfigs);
  }

  /**
   * Get the list of configurations for a specific addon. If no configurations are available, a list
   * for the specified ID os added to the addons configuration so you may add, change or delete to
   * your hearts content.
   *
   * @param addonId The ID of the addon.
   * @return Configuration for the specified addon.
   * @author Griefed
   */
  public Optional<ArrayList<CommentedConfig>> getAddonConfigs(@NotNull String addonId) {
    return Optional.ofNullable(addonsConfigs.get(addonId));
  }

  /**
   * Get the configurations for the specified addon ID. If no list of configurations is present for
   * the requested ID, one is created, so it can then be adjusted as you wish.
   *
   * @param addonId The ID identifying the addon for which to get the list of configurations.
   * @return Configurations for the specified addon ID.
   * @author Griefed
   */
  public ArrayList<CommentedConfig> getOrCreateAddonConfigList(@NotNull String addonId) {
    if (!addonsConfigs.containsKey(addonId)) {
      addonsConfigs.put(addonId, new ArrayList<>(100));
    }
    return addonsConfigs.get(addonId);
  }

  /**
   * Getter for the suffix of the server pack to be generated.
   *
   * @return Suffix for the server pack to be generated.
   * @author Griefed
   */
  public String getServerPackSuffix() {
    return serverPackSuffix;
  }

  /**
   * Setter for the suffix of the server pack to be generated
   *
   * @param serverPackSuffix The suffix of the server pack to be generated.
   * @author Griefed
   */
  public void setServerPackSuffix(@NotNull String serverPackSuffix) {
    this.serverPackSuffix = serverPackSuffix;
  }

  /**
   * Getter for a list of clientside-only mods to exclude from server pack.
   *
   * @return Clientside-only mods.
   * @author Griefed
   */
  public List<String> getClientMods() {
    return clientMods;
  }

  /**
   * Setter for the list of clientside-only mods to exclude from server pack.
   *
   * @param newClientMods The new list of clientside-only mods to store.
   * @author Griefed
   */
  public void setClientMods(@NotNull List<String> newClientMods) {
    this.clientMods.clear();
    newClientMods.removeIf(entry -> entry.matches("\\s+") || entry.isEmpty());
    this.clientMods.addAll(newClientMods);
  }

  /**
   * Getter for the list of directories in the modpack to copy to the server pack.
   *
   * @return Directories to copy to the server pack.
   * @author Griefed
   */
  public List<String> getCopyDirs() {
    return copyDirs;
  }

  /**
   * Setter for the list of directories in the modpack to copy to the server pack.
   *
   * @param newCopyDirs The new list of directories to include in server pack to store.
   * @author Griefed
   */
  public void setCopyDirs(@NotNull List<String> newCopyDirs) {
    this.copyDirs.clear();
    newCopyDirs.removeIf(
        entry ->
            entry.equalsIgnoreCase("server_pack") || entry.matches("\\s+") || entry.isEmpty());
    this.copyDirs.addAll(newCopyDirs);
  }

  /**
   * Getter for the path to the modpack directory.
   *
   * @return Path to the modpack directory.
   * @author Griefed
   */
  public String getModpackDir() {
    return modpackDir;
  }

  /**
   * Setter for the path to the modpack directory. Replaces any occurrences of \ with /.
   *
   * @param newModpackDir The new modpack directory path to store.
   * @author Griefed
   */
  public void setModpackDir(@NotNull String newModpackDir) {
    this.modpackDir = newModpackDir;
  }

  /**
   * Getter for the version of Minecraft used by the modpack.
   *
   * @return Minecraft version used in the modpack.
   * @author Griefed
   */
  public String getMinecraftVersion() {
    return minecraftVersion;
  }

  /**
   * Setter for the Minecraft version used by the modpack.
   *
   * @param newMinecraftVersion The new Minecraft version to store.
   * @author Griefed
   */
  public void setMinecraftVersion(@NotNull String newMinecraftVersion) {
    this.minecraftVersion = newMinecraftVersion;
  }

  /**
   * Getter for the modloader used by the modpack.
   *
   * @return Modloader used by the modpack.
   * @author Griefed
   */
  public String getModLoader() {
    return modLoader;
  }

  /**
   * Setter for the modloader used by the modpack.
   *
   * @param newModLoader The new modloader to store.
   * @author Griefed
   */
  public void setModLoader(@NotNull String newModLoader) {
    if (newModLoader.toLowerCase().matches("^forge$")) {

      this.modLoader = "Forge";

    } else if (newModLoader.toLowerCase().matches("^fabric$")) {

      this.modLoader = "Fabric";

    } else if (newModLoader.toLowerCase().matches("^quilt$")) {

      this.modLoader = "Quilt";

    } else if (newModLoader.toLowerCase().matches("^legacyfabric$")) {

      this.modLoader = "LegacyFabric";
    }
  }

  /**
   * Getter for the version of the modloader used by the modpack.
   *
   * @return The version of the modloader used by the modpack.
   * @author Griefed
   */
  public String getModLoaderVersion() {
    return modLoaderVersion;
  }

  /**
   * Setter for the version of the modloader used by the modpack.
   *
   * @param newModLoaderVersion The new modloader version to store.
   * @author Griefed
   */
  public void setModLoaderVersion(@NotNull String newModLoaderVersion) {
    this.modLoaderVersion = newModLoaderVersion;
  }

  /**
   * Getter for whether the modloader server installation should be included.
   *
   * @return {@code true} if the server should be installed.
   * @author Griefed
   */
  public boolean isServerInstallationDesired() {
    return includeServerInstallation;
  }

  /**
   * Setter for whether the modloader server installation should be included.
   *
   * @param newIncludeServerInstallation The new boolean to store.
   * @author Griefed
   */
  public void setIncludeServerInstallation(boolean newIncludeServerInstallation) {
    this.includeServerInstallation = newIncludeServerInstallation;
  }

  /**
   * Getter for whether the server-icon.png should be included in the server pack.
   *
   * @return {@code true} if the icon should be included.
   * @author Griefed
   */
  public boolean isServerIconInclusionDesired() {
    return includeServerIcon;
  }

  /**
   * Setter for whether the server-icon.png should be included in the server pack.
   *
   * @param newIncludeServerIcon The new boolean to store.
   * @author Griefed
   */
  public void setIncludeServerIcon(boolean newIncludeServerIcon) {
    this.includeServerIcon = newIncludeServerIcon;
  }

  /**
   * Getter for whether the server.properties should be included in the server pack.
   *
   * @return {@code true} if the properties should be included.
   * @author Griefed
   */
  public boolean isServerPropertiesInclusionDesired() {
    return includeServerProperties;
  }

  /**
   * Setter for whether the server.properties should be included in the server pack.
   *
   * @param newIncludeServerProperties The new boolean to store.
   * @author Griefed
   */
  public void setIncludeServerProperties(boolean newIncludeServerProperties) {
    this.includeServerProperties = newIncludeServerProperties;
  }

  /**
   * Getter for whether a ZIP-archive of the server pack should be created.
   *
   * @return {@code true} if the ZIP-archive should be created.
   * @author Griefed
   */
  public boolean isZipCreationDesired() {
    return includeZipCreation;
  }

  /**
   * Setter for whether a ZIP-archive of the server pack should be created.
   *
   * @param newIncludeZipCreation The new boolean to store.
   * @author Griefed
   */
  public void setIncludeZipCreation(boolean newIncludeZipCreation) {
    this.includeZipCreation = newIncludeZipCreation;
  }

  /**
   * Getter for the Java arguments with which the start-scripts will be generated.
   *
   * @return Returns the Java arguments with which the start-scripts will be generated.
   * @author Griefed
   */
  public String getJavaArgs() {
    return javaArgs;
  }

  /**
   * Setter for the Java arguments with which the start-scripts will be generated.
   *
   * @param javaArgs Sets the Java arguments with which the start-scripts will be generated.
   * @author Griefed
   */
  public void setJavaArgs(@NotNull String javaArgs) {
    this.javaArgs = javaArgs;
  }

  /**
   * Getter for the JsonNode containing all information about the modpack. May be from various *
   * sources, so be careful when using this.
   *
   * @return The JsonNode containing all information about the modpack.
   * @author Griefed
   */
  public JsonNode getModpackJson() {
    return modpackJson;
  }

  /**
   * Setter for the JsonNode containing all information about the modpack. May be from various
   * sources, so be careful when using this.
   *
   * @param modpackJson The JsonNode containing all information about the modpack.
   * @author Griefed
   */
  public void setModpackJson(@NotNull JsonNode modpackJson) {
    this.modpackJson = modpackJson;
  }

  /**
   * Getter for the name of the CurseForge project.
   *
   * @return The name of the CurseForge project.
   * @author Griefed
   */
  public String getProjectName() {
    return projectName;
  }

  /**
   * Setter for the name of the CurseForge project.
   *
   * @param projectName The name of the CurseForge project.
   * @author Griefed
   */
  public void setProjectName(@NotNull String projectName) {
    this.projectName = projectName;
  }

  /**
   * Getter for the name of the CurseForge project file.
   *
   * @return The name of the CurseForge project file.
   * @author Griefed
   */
  public String getFileName() {
    return fileName;
  }

  /**
   * Setter for the name of the CurseForge project file.
   *
   * @param fileName The name of the CurseForge project file.
   * @author Griefed
   */
  public void setFileName(@NotNull String fileName) {
    this.fileName = fileName;
  }

  /**
   * Getter for the disk-name of the CurseForge project file.
   *
   * @return The disk-name of the CurseForge project file.
   * @author Griefed
   */
  public String getFileDiskName() {
    return fileDiskName;
  }

  /**
   * Setter for the disk-name of the CurseForge project file.
   *
   * @param fileName The disk-name of the CurseForge project file.
   * @author Griefed
   */
  public void setFileDiskName(@NotNull String fileName) {
    this.fileDiskName = fileName;
  }

  /**
   * Getter for the path to the server-icon.png to include in the server pack.
   *
   * @return Returns the path to the server-icon.png.
   * @author Griefed
   */
  public String getServerIconPath() {
    return serverIconPath;
  }

  /**
   * Setter for the path of the server-icon.png to include in the server pack.
   *
   * @param serverIconPath The path to the server-icon.png to include in the server pack.
   * @author Griefed
   */
  public void setServerIconPath(@NotNull String serverIconPath) {
    this.serverIconPath = serverIconPath;
  }

  /**
   * Getter for the path of the server.properties to include in the server pack.
   *
   * @return Returns the path to the server.properties to include in the server pack.
   * @author Griefed
   */
  public String getServerPropertiesPath() {
    return serverPropertiesPath;
  }

  /**
   * Setter for the path of the server.properties to include in the server pack.
   *
   * @param serverPropertiesPath The path to the server.properties to include in the server pack.
   * @author Griefed
   */
  public void setServerPropertiesPath(@NotNull String serverPropertiesPath) {
    this.serverPropertiesPath = serverPropertiesPath;
  }

  /**
   * Getter for the script settings used during script creation.
   *
   * @return Map of all script settings and their values.
   * @author Griefed
   */
  public HashMap<String, String> getScriptSettings() {
    return scriptSettings;
  }

  /**
   * Putter for the script settings used during script creation. All key-value pairs from the passed
   * hashmap are put into this models hashmap.
   *
   * @param settings Key-value pairs to be used in script creation.
   * @author Griefed
   */
  public void setScriptSettings(@NotNull HashMap<String, String> settings) {
    this.scriptSettings.clear();
    this.scriptSettings.putAll(settings);
  }

  @Override
  public String toString() {
    return "ConfigurationModel{" +
        "clientMods=" + clientMods +
        ", copyDirs=" + copyDirs +
        ", scriptSettings=" + scriptSettings +
        ", addonsConfigs=" + addonsConfigs +
        ", modpackDir='" + modpackDir + '\'' +
        ", minecraftVersion='" + minecraftVersion + '\'' +
        ", modLoader='" + modLoader + '\'' +
        ", modLoaderVersion='" + modLoaderVersion + '\'' +
        ", javaArgs='" + javaArgs + '\'' +
        ", serverPackSuffix='" + serverPackSuffix + '\'' +
        ", serverIconPath='" + serverIconPath + '\'' +
        ", serverPropertiesPath='" + serverPropertiesPath + '\'' +
        ", includeServerInstallation=" + includeServerInstallation +
        ", includeServerIcon=" + includeServerIcon +
        ", includeServerProperties=" + includeServerProperties +
        ", includeZipCreation=" + includeZipCreation +
        ", modpackJson=" + modpackJson +
        ", projectName='" + projectName + '\'' +
        ", fileName='" + fileName + '\'' +
        ", fileDiskName='" + fileDiskName + '\'' +
        '}';
  }
}
