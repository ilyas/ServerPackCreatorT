/* Copyright (C) 2022  Griefed
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * The full license can be found at https:github.com/Griefed/ServerPackCreator/blob/main/LICENSE
 */
/**
 * The Swing-GUI for ServerPackCreator. The GUI itself holds little to no actual ServerPackCreator
 * logic, but instead focuses on accessing the underlying logic to allow a user to configure and
 * generate a server pack. This package creates the complete GUI which can then be used by a given
 * user. Entrypoint is
 * {@link de.griefed.serverpackcreator.swing.ServerPackCreatorWindow#mainGUI()}.
 *
 * @author Griefed
 */
package de.griefed.serverpackcreator.swing;
