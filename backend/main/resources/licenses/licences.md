
#Licenses
##Dependency License Report
_2022-10-14 20:41:06 MESZ_
## Apache License, Version 2.0

**1** **Group:** `com.github.vatbub` **Name:** `mslinks` **Version:** `1.0.5` 
> - **POM Project URL**: [https://github.com/vatbub/mslinks](https://github.com/vatbub/mslinks)
> - **POM License**: Apache License, Version 2.0 - [https://www.apache.org/licenses/LICENSE-2.0](https://www.apache.org/licenses/LICENSE-2.0)

**2** **Group:** `net.lingala.zip4j` **Name:** `zip4j` **Version:** `2.11.2` 
> - **Manifest License**: Apache License, Version 2.0 (Not Packaged)
> - **POM Project URL**: [https://github.com/srikanth-lingala/zip4j](https://github.com/srikanth-lingala/zip4j)
> - **POM License**: Apache License, Version 2.0 - [https://www.apache.org/licenses/LICENSE-2.0](https://www.apache.org/licenses/LICENSE-2.0)

**3** **Group:** `org.apache.activemq` **Name:** `artemis-jms-server` **Version:** `2.19.1` 
> - **POM License**: Apache License, Version 2.0 - [https://www.apache.org/licenses/LICENSE-2.0](https://www.apache.org/licenses/LICENSE-2.0)
> - **Embedded license files**: [artemis-jms-server-2.19.1.jar/META-INF/LICENSE](artemis-jms-server-2.19.1.jar/META-INF/LICENSE) 
    - [artemis-jms-server-2.19.1.jar/META-INF/NOTICE](artemis-jms-server-2.19.1.jar/META-INF/NOTICE)

**4** **Group:** `org.apache.logging.log4j` **Name:** `log4j-slf4j-impl` **Version:** `2.19.0` 
> - **Manifest Project URL**: [https://www.apache.org/](https://www.apache.org/)
> - **Manifest License**: Apache License, Version 2.0 (Not Packaged)
> - **POM License**: Apache License, Version 2.0 - [https://www.apache.org/licenses/LICENSE-2.0](https://www.apache.org/licenses/LICENSE-2.0)
> - **Embedded license files**: [log4j-slf4j-impl-2.19.0.jar/META-INF/LICENSE](log4j-slf4j-impl-2.19.0.jar/META-INF/LICENSE) 
    - [log4j-slf4j-impl-2.19.0.jar/META-INF/NOTICE](log4j-slf4j-impl-2.19.0.jar/META-INF/NOTICE)

**5** **Group:** `org.apache.logging.log4j` **Name:** `log4j-core` **Version:** `2.19.0` 
> - **Manifest Project URL**: [https://www.apache.org/](https://www.apache.org/)
> - **Manifest License**: Apache License, Version 2.0 (Not Packaged)
> - **POM License**: Apache License, Version 2.0 - [https://www.apache.org/licenses/LICENSE-2.0](https://www.apache.org/licenses/LICENSE-2.0)
> - **Embedded license files**: [log4j-core-2.19.0.jar/META-INF/LICENSE](log4j-core-2.19.0.jar/META-INF/LICENSE) 
    - [log4j-core-2.19.0.jar/META-INF/NOTICE](log4j-core-2.19.0.jar/META-INF/NOTICE)

**6** **Group:** `org.apache.logging.log4j` **Name:** `log4j-web` **Version:** `2.19.0` 
> - **Manifest Project URL**: [https://www.apache.org/](https://www.apache.org/)
> - **Manifest License**: Apache License, Version 2.0 (Not Packaged)
> - **POM License**: Apache License, Version 2.0 - [https://www.apache.org/licenses/LICENSE-2.0](https://www.apache.org/licenses/LICENSE-2.0)
> - **Embedded license files**: [log4j-web-2.19.0.jar/META-INF/LICENSE](log4j-web-2.19.0.jar/META-INF/LICENSE) 
    - [log4j-web-2.19.0.jar/META-INF/NOTICE](log4j-web-2.19.0.jar/META-INF/NOTICE)

**7** **Group:** `org.apache.logging.log4j` **Name:** `log4j-api` **Version:** `2.19.0` 
> - **Manifest Project URL**: [https://www.apache.org/](https://www.apache.org/)
> - **Manifest License**: Apache License, Version 2.0 (Not Packaged)
> - **POM License**: Apache License, Version 2.0 - [https://www.apache.org/licenses/LICENSE-2.0](https://www.apache.org/licenses/LICENSE-2.0)
> - **Embedded license files**: [log4j-api-2.19.0.jar/META-INF/LICENSE](log4j-api-2.19.0.jar/META-INF/LICENSE) 
    - [log4j-api-2.19.0.jar/META-INF/NOTICE](log4j-api-2.19.0.jar/META-INF/NOTICE)

**8** **Group:** `org.jgroups` **Name:** `jgroups` **Version:** `5.2.6.Final` 
> - **POM Project URL**: [http://www.jgroups.org](http://www.jgroups.org)
> - **POM License**: Apache License, Version 2.0 - [https://www.apache.org/licenses/LICENSE-2.0](https://www.apache.org/licenses/LICENSE-2.0)
> - **Embedded license files**: [jgroups-5.2.6.Final.jar/LICENSE](jgroups-5.2.6.Final.jar/LICENSE) 
    - [jgroups-5.2.6.Final.jar/README](jgroups-5.2.6.Final.jar/README)

**9** **Group:** `org.pf4j` **Name:** `pf4j` **Version:** `3.7.0` 
> - **POM License**: Apache License, Version 2.0 - [https://www.apache.org/licenses/LICENSE-2.0](https://www.apache.org/licenses/LICENSE-2.0)

**10** **Group:** `org.springframework.boot` **Name:** `spring-boot-devtools` **Version:** `2.7.4` 
> - **POM Project URL**: [https://spring.io/projects/spring-boot](https://spring.io/projects/spring-boot)
> - **POM License**: Apache License, Version 2.0 - [https://www.apache.org/licenses/LICENSE-2.0](https://www.apache.org/licenses/LICENSE-2.0)
> - **Embedded license files**: [spring-boot-devtools-2.7.4.jar/META-INF/LICENSE.txt](spring-boot-devtools-2.7.4.jar/META-INF/LICENSE.txt) 
    - [spring-boot-devtools-2.7.4.jar/META-INF/NOTICE.txt](spring-boot-devtools-2.7.4.jar/META-INF/NOTICE.txt)

**11** **Group:** `org.springframework.boot` **Name:** `spring-boot-starter-log4j2` **Version:** `2.7.4` 
> - **POM Project URL**: [https://spring.io/projects/spring-boot](https://spring.io/projects/spring-boot)
> - **POM License**: Apache License, Version 2.0 - [https://www.apache.org/licenses/LICENSE-2.0](https://www.apache.org/licenses/LICENSE-2.0)
> - **Embedded license files**: [spring-boot-starter-log4j2-2.7.4.jar/META-INF/LICENSE.txt](spring-boot-starter-log4j2-2.7.4.jar/META-INF/LICENSE.txt) 
    - [spring-boot-starter-log4j2-2.7.4.jar/META-INF/NOTICE.txt](spring-boot-starter-log4j2-2.7.4.jar/META-INF/NOTICE.txt)

**12** **Group:** `org.springframework.boot` **Name:** `spring-boot-starter-web` **Version:** `2.7.4` 
> - **POM Project URL**: [https://spring.io/projects/spring-boot](https://spring.io/projects/spring-boot)
> - **POM License**: Apache License, Version 2.0 - [https://www.apache.org/licenses/LICENSE-2.0](https://www.apache.org/licenses/LICENSE-2.0)
> - **Embedded license files**: [spring-boot-starter-web-2.7.4.jar/META-INF/LICENSE.txt](spring-boot-starter-web-2.7.4.jar/META-INF/LICENSE.txt) 
    - [spring-boot-starter-web-2.7.4.jar/META-INF/NOTICE.txt](spring-boot-starter-web-2.7.4.jar/META-INF/NOTICE.txt)

**13** **Group:** `org.springframework.boot` **Name:** `spring-boot-starter-data-jpa` **Version:** `2.7.4` 
> - **POM Project URL**: [https://spring.io/projects/spring-boot](https://spring.io/projects/spring-boot)
> - **POM License**: Apache License, Version 2.0 - [https://www.apache.org/licenses/LICENSE-2.0](https://www.apache.org/licenses/LICENSE-2.0)
> - **Embedded license files**: [spring-boot-starter-data-jpa-2.7.4.jar/META-INF/LICENSE.txt](spring-boot-starter-data-jpa-2.7.4.jar/META-INF/LICENSE.txt) 
    - [spring-boot-starter-data-jpa-2.7.4.jar/META-INF/NOTICE.txt](spring-boot-starter-data-jpa-2.7.4.jar/META-INF/NOTICE.txt)

**14** **Group:** `org.springframework.boot` **Name:** `spring-boot-starter-artemis` **Version:** `2.7.4` 
> - **POM Project URL**: [https://spring.io/projects/spring-boot](https://spring.io/projects/spring-boot)
> - **POM License**: Apache License, Version 2.0 - [https://www.apache.org/licenses/LICENSE-2.0](https://www.apache.org/licenses/LICENSE-2.0)
> - **Embedded license files**: [spring-boot-starter-artemis-2.7.4.jar/META-INF/LICENSE.txt](spring-boot-starter-artemis-2.7.4.jar/META-INF/LICENSE.txt) 
    - [spring-boot-starter-artemis-2.7.4.jar/META-INF/NOTICE.txt](spring-boot-starter-artemis-2.7.4.jar/META-INF/NOTICE.txt)

**15** **Group:** `org.xerial` **Name:** `sqlite-jdbc` **Version:** `3.39.3.0` 
> - **Manifest License**: Apache License, Version 2.0 (Not Packaged)
> - **POM Project URL**: [https://github.com/xerial/sqlite-jdbc](https://github.com/xerial/sqlite-jdbc)
> - **POM License**: Apache License, Version 2.0 - [https://www.apache.org/licenses/LICENSE-2.0](https://www.apache.org/licenses/LICENSE-2.0)
> - **Embedded license files**: [sqlite-jdbc-3.39.3.0.jar/META-INF/maven/org.xerial/sqlite-jdbc/LICENSE](sqlite-jdbc-3.39.3.0.jar/META-INF/maven/org.xerial/sqlite-jdbc/LICENSE) 
    - [sqlite-jdbc-3.39.3.0.jar/META-INF/maven/org.xerial/sqlite-jdbc/LICENSE.zentus](sqlite-jdbc-3.39.3.0.jar/META-INF/maven/org.xerial/sqlite-jdbc/LICENSE.zentus)

## GNU Lesser General Public License v3.0

**16** **Group:** `com.electronwill.night-config` **Name:** `toml` **Version:** `3.6.6` 
> - **POM Project URL**: [https://github.com/TheElectronWill/Night-Config](https://github.com/TheElectronWill/Night-Config)
> - **POM License**: GNU Lesser General Public License v3.0 - [https://www.gnu.org/licenses/lgpl-3.0.txt](https://www.gnu.org/licenses/lgpl-3.0.txt)

## MIT License

**17** **Group:** `de.griefed` **Name:** `larsonscanner` **Version:** `1.0.4` 
> - **POM Project URL**: [https://git.griefed.de/Griefed/LarsonScanner.git](https://git.griefed.de/Griefed/LarsonScanner.git)
> - **POM License**: MIT License - [https://opensource.org/licenses/MIT](https://opensource.org/licenses/MIT)
> - **Embedded license files**: [larsonscanner-1.0.4.jar/LICENSE](larsonscanner-1.0.4.jar/LICENSE)

**18** **Group:** `de.griefed` **Name:** `versionchecker` **Version:** `1.1.0` 
> - **POM Project URL**: [https://git.griefed.de/Griefed/VersionChecker.git](https://git.griefed.de/Griefed/VersionChecker.git)
> - **POM License**: MIT License - [https://opensource.org/licenses/MIT](https://opensource.org/licenses/MIT)

**19** **Group:** `io.github.vincenzopalazzo` **Name:** `material-ui-swing` **Version:** `1.1.4` 
> - **POM Project URL**: [https://github.com/vincenzopalazzo/material-ui-swing](https://github.com/vincenzopalazzo/material-ui-swing)
> - **POM License**: MIT License - [https://opensource.org/licenses/MIT](https://opensource.org/licenses/MIT)

## PUBLIC DOMAIN

**20** **Group:** `com.github.gwenn` **Name:** `sqlite-dialect` **Version:** `0.1.2` 
> - **POM Project URL**: [https://github.com/gwenn/sqlite-dialect](https://github.com/gwenn/sqlite-dialect)
> - **POM License**: PUBLIC DOMAIN - [http://unlicense.org/](http://unlicense.org/)


